﻿using Microsoft.EntityFrameworkCore;
using SelfiesWookies.core.domain;
using SelfiesWookies.core.infrastructure.TypeConfigurations;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfiesWookies.core.infrastructure.Data
{
    public class SelfieContext: DbContext
    {
        #region Constructor

        public SelfieContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected SelfieContext()
        {
        }
        #endregion

        #region Internal method
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new SelfiesEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WookiesEntityTypeConfiguration());
        }
        #endregion

        #region Properties
        public DbSet<Selfie> Selfies { get; set; }
        public DbSet<Wookie> Wookies { get; set; }
        #endregion
    }
}
