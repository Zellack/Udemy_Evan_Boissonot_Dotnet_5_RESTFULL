﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SelfiesWookies.core.domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfiesWookies.core.infrastructure.TypeConfigurations
{
    class SelfiesEntityTypeConfiguration : IEntityTypeConfiguration<Selfie>
    {
        public void Configure(EntityTypeBuilder<Selfie> builder)
        {
            builder.ToTable("Selfie");

            builder.HasKey(item => item.Id);
            builder.HasOne(item => item.Wookie)
                   .WithMany(item => item.Selfies);
        }
    }
}
