﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using SelfiesWookies.core.domain;
using SelfiesWookies.core.infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace Selfie_wookie_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SelfiesController : ControllerBase
    {
        private readonly SelfieContext selfieContext;

        #region Constructor
        public SelfiesController(SelfieContext selfieContext)
        {
            this.selfieContext = selfieContext;
        }
        #endregion

        #region Public methods
        [HttpGet]
        public IActionResult FindAllSelfie()
        {
            //var result = Enumerable.Range(1, 10).Select(item => new Selfie() { Id = item });
            var model = this.selfieContext.Selfies
                                          .Include(item => item.Wookie)
                                          .Select(item => new
                                          {
                                              Id = item.Id,
                                              Title = item.Title,
                                              ImagePath = item.ImagePath,
                                              Wookie = new
                                              {
                                                  Id = item.Wookie.Id,
                                                  FirstName = item.Wookie.FirstName
                                              }
                                          }) ;
            return this.Ok(model.ToList());
        }
        #endregion
    }
}
